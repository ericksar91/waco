# WACO

The present project is the technical test of WACO that consists in building docker images to run the application that is in the git https://gitlab.com/xx1196/sre

## Manual docker image construction commands with docker-compose

Commands to create environment:

```sh
docker-compose up --build -d
```
Commands to destroy environment:
```sh
docker-compose down -v
```
Environment validation commands:
```sh
docker-compose up --build -d
docker-compose exec app sh -x laravel-config.sh
docker exec app php artisan test
```
Verify the deployment by navigating to your server address in your preferred browser.

```sh
127.0.0.1:80
http://localhost/
```


## Deployment with template on cloudformation AWS

Inside the cloudformation folder is a yml. this file we load it in a stack in the cloudformation service in AWS. we only have to configure the key that will use the EC2 instance and create the instance and a security group. at the end of the deployment has a boostrap that makes the installation of docker and docker-compose that will perform the deployment of the complete environment leaving it functional.